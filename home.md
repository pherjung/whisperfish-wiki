Whisperfish is a third-party, unofficial client for [Signal](https://signal.org/), the private messenger, for SailfishOS.
It is available on [OpenRepos](https://openrepos.net/node/11046/), or as [master-branch builds on Gitlab](https://gitlab.com/rubdos/whisperfish/-/packages/).

You have a question? Please check our **[Frequently Asked Questions](Frequenty-Asked-Questions)**. If the FAQ does not answer your question, join us on [Matrix or Libera.Chat](Contact-Us), make [an issue on Gitlab](https://gitlab.com/rubdos/whisperfish/-/issues) or [send me an email, Signal or text message](https://www.rubdos.be/about/)!

If you want to edit this wiki, you can do so through [this mirror repository](https://gitlab.com/rubdos/whisperfish-wiki/).

## Notably unimplemented features

- [Contact discovery and sharing](https://gitlab.com/rubdos/whisperfish/-/issues/133), see also https://github.com/Michael-F-Bryan/libsignal-service-rs/pull/52.
- [GroupV2](https://gitlab.com/rubdos/whisperfish/-/issues/86)
- [UUID-only devices](https://gitlab.com/rubdos/whisperfish/-/issues/80),
    although these do not exist yet (I think).
- [Group management](https://gitlab.com/rubdos/whisperfish/-/issues/100) (creating and modifying groups)
- [Linking devices](https://gitlab.com/rubdos/whisperfish/-/issues/97) (i.e., Signal Desktop)
- [Session reset](https://gitlab.com/rubdos/whisperfish/-/issues/114) and [fingerprint changes](https://gitlab.com/rubdos/whisperfish/-/issues/121)

## Known issues

- Sending a message to a contact that has registered newly after Februari 2021
    (https://gitlab.com/rubdos/whisperfish/-/issues/237).
  This is due to Signal stopping the provisioning via E164-based pre-key paths
  in their API.
  Symptoms: "413 Rate Limit Exceeded" and "404 Not Found" both on `/v2/keys/+...`

### Specifically to SailfishOS 4.0

Exhaustive list of issues at https://gitlab.com/rubdos/whisperfish/-/issues?label_name%5B%5D=SailfishOS-4.0

- Contact names disappear after a while/do not show up (https://gitlab.com/rubdos/whisperfish/-/issues/231) and contact selection (https://gitlab.com/rubdos/whisperfish/-/issues/232).
  This is due to Jolla's new Firejail. !118 resolves this, this should be in beta.1.
- Registration is impossible
  (https://gitlab.com/rubdos/whisperfish/-/issues/229).

## Debugging and logging

We often ask for a trace log when encountering a new issue.
Most interesting data can be obtained by running `harbour-whisperfish -v` or
`harbour-whisperfish --verbose` on the CLI, either on fingerterm or via SSH,
as of Whisperfish `beta.1` or `dev.b1016`.
For very obscure bugs (related to CPU usage, hangs, ...), you may need
`RUST_LOG=trace harbour-whisperfish` instead.
This is extremely verbose, because it prints trace-logs for *every* dependency.

### Censor your logs

Logs contain sensitive information (https://gitlab.com/rubdos/whisperfish/-/issues/124).
Four things to consider.

1. Look for your phone number in the log, throw these out, or mask them
(e.g. `+324xxxxx`).

2. Profile keys and group IDs. Look for `GroupContext` and yeet out the `id` field.
   Look for `GroupContextV2` and throw out the `master_key`.
   Look for `profile_key` and throw it out.

3. Also look out specifically for this line:
```
[2021-02-11T08:43:59Z TRACE libsignal_service_actix::websocket] Will start websocket at Url { scheme: "wss", host: Some(Domain("textsecure-service.whispersystems.org")), port: None, path: "/v1/websocket/", query: Some("login=9bad15b5-dca3-418a-9949-7ca357b7fe47&password=xxxxxx"), fragment: None }

```
and **throw the password out**.
The log can contain this line multiple times, which indicates some connection
failure.

4. You may also want to look for the UUIDs (like `9bad15b5-dca3-418a-9949-7ca357b7fe47`) and yeet these out.
  They are your personal ID, or your friends personal ID on Signal.

Some useful regexes:

- uuid `[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}`
- A swiss phone `41[0-9]{9}`
