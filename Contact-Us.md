The main developer of Whisperfish is Ruben De Smet.
You can [contact me directly](https://www.rubdos.be/about/) if you really like,
but there are other ways to get in touch.

## Matrix

[Matrix](https://matrix.org/) is a federated network of chat rooms.
You need an account on some server in order to use it.
Find us on [#whisperfish:rubdos.be](https://matrix.to/#/#whisperfish:rubdos.be), when you have an account!
The Matrix room is bridged to Libera.Chat (see below).
We also have [#whisperfish-offtopic:rubdos.be](https://matrix.to/#/#whisperfish-offtopic:rubdos.be),
for if you're in for a random chat!
This channel is also bridged to Libera.Chat.

## Libera.Chat's IRC

Find `rubdos` on Libera.Chat, either in the SailfishOS channel, or the
[#whisperfish](https://web.libera.chat/#whisperfish) channel.
We also have [#whisperfish-offtopic](https://web.libera.chat/#whisperfish-offtopic),
for if you're in for a random chat!
